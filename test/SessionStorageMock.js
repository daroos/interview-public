export class SessionStorageMock {
  getItem(key) {
    if (key === 'pageFilter') {
      return 0;
    }

    if (key === 'sort') {
      return 'authors-name';
    }
  }

  setItem(key, value) {
    return null;
  }
}