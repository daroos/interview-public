import { SessionStorageMock } from "./SessionStorageMock";
import { BookRepository } from "../src/js/BookRepository";
import { ViewRendererMock } from "./ViewRendererMock";
import {ApiClient} from "../src/js/ApiClient";

const assert = require('assert');

describe('Sorting', () => {
  const booksInput = [
    {
      title: 'JavaScript: The Good Parts',
      author: 'Douglas Crockford',
      releaseDate: '12/2008',
      pages: 172
    },
    {
      title: 'Learning JavaScript Design Patterns',
      author: 'Addy Osmani',
      releaseDate: '08/2012',
      pages: 254
    },
    {
      title: 'Programming JavaScript Applications',
      author: 'Eric Elliott',
      releaseDate: '07/2014',
      pages: 500
    },
    {
      title: 'JavaScript Enlightenment',
      author: 'Cody Lindley',
      releaseDate: '12/2012',
      pages: 166
    }
  ];

  const apiClient = new ApiClient();

  const books = booksInput.map(book => {
    return apiClient.addSortablePropertiesToBooks(book);
  });

  let bookRepository = new BookRepository(new SessionStorageMock(), new ViewRendererMock());

  it('by author\'s name should return filtered books in right order', () => {

    const filteredBooks = bookRepository.filterAndSortBooks(books, 0, 'authors-name');

    assert.equal(books[0], filteredBooks[0]);
    assert.equal(books[1], filteredBooks[3]);
    assert.equal(books[2], filteredBooks[1]);
    assert.equal(books[3], filteredBooks[2]);
  });

  it('by publish date should return filtered books in right order', () => {

    const filteredBooks = bookRepository.filterAndSortBooks(books, 0, 'publish-date');

    assert.equal(books[0], filteredBooks[0]);
    assert.equal(books[1], filteredBooks[1]);
    assert.equal(books[2], filteredBooks[3]);
    assert.equal(books[3], filteredBooks[2]);
  });

  it('by page count should return filtered books in right order', () => {

    const filteredBooks = bookRepository.filterAndSortBooks(books, 0, 'page-count');

    assert.equal(books[0], filteredBooks[1]);
    assert.equal(books[1], filteredBooks[2]);
    assert.equal(books[2], filteredBooks[3]);
    assert.equal(books[3], filteredBooks[0]);
  });

  describe('and filtering', () => {
    it('by page count and more than 200 pages should return filtered books in right order and amount', () => {

      const filteredBooks = bookRepository.filterAndSortBooks(books, 200, 'page-count');

      assert.equal(books[1], filteredBooks[0]);
      assert.equal(books[2], filteredBooks[1]);
      assert.equal(filteredBooks.length, 2);
    });

    it('by page count and more than 300 pages should return filtered books in right order and amount', () => {

      const filteredBooks = bookRepository.filterAndSortBooks(books, 300, 'page-count');

      assert.equal(books[2], filteredBooks[0]);
      assert.equal(filteredBooks.length, 1);
    });

    it('by page count and more than 600 pages should return no books', () => {

      const filteredBooks = bookRepository.filterAndSortBooks(books, 600, 'page-count');

      assert.deepEqual([], filteredBooks);
    });
  });
});