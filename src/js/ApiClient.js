export class ApiClient {
  getAllBooks() {
    return fetch('http://localhost:3000/books.json')
      .then((response) => {
        return response.json();
      }).then(books => {
        return books.map(book => {
          return this.addSortablePropertiesToBooks(book);
        });
      });
  }

  addSortablePropertiesToBooks(book) {
    book.releaseDate = new Date(book.releaseDate.split('/').reverse().join('-'));
    book.authorsSortableSurname = book.author.toLowerCase().split(' ')[1];
    return book;
  }
}