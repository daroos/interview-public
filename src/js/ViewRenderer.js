export class ViewRenderer {
  renderBooks(books) {

    let html = books.reduce((prev, book) => {
      const formatedReleaseDate = `${book.releaseDate.getMonth()}/${book.releaseDate.getFullYear()}`;

      const template = `
            <li class="single-book">
                <div class="book-cover">
                    <a class="large-image popup" href="${book.cover.large}">
                        <img class="small-image" src="${book.cover.small}">
                    </a>
                </div>
    
                <div class="book-description">
                    <h3 class="book-title">${book.title}</h3>
                    <div class="book-description-divider"></div>
                    <div class="book-author">By ${book.author}</div>
                    <dl>
                        <dt class="book-description-category">Release Date:</dt>
                        <dd class="book-description-value release-date">${formatedReleaseDate}</dd>
                        <dt class="book-description-category">Pages:</dt>
                        <dd class="book-description-value pages">${book.pages}</dd>
                        <dt class="book-description-category">Link:</dt>
                        <dd class="book-description-value"><a class="link-to-shop" href="${book.link}">shop</a></dd>
                    </dl>
                </div>
            </li>`;

      return `${prev}${template}`;
    }, '');

    document.getElementById('book-list').innerHTML = `<ol>${html}</ol>`;

    this.addEventListenersForPopups();
  }

  addEventListenersForPopups() {
    const thumbnailLinks = document.getElementsByClassName('popup');

    Array.from(thumbnailLinks).forEach((link) => {
      link.addEventListener('click', (event) => {
        event.preventDefault();
        this.showPopup(event.target.parentNode.href);
      });
    });
  }

  showPopup(imageUrl) {
    const template = `
              <div class="image">
                  <img src="${imageUrl}" alt="Large image" class="large-image">
                  <button id="close-overlay" class="close"><img src="img/close.png" alt=""></button>
              </div>`;

    let el = document.createElement('div');
    el.className = 'overlay';
    el.innerHTML = template;

    document.getElementsByTagName('body')[0].appendChild(el);

    document.getElementById('close-overlay').addEventListener('click', () => {
      this.destroyPopup();
    });
  }

  destroyPopup() {
    const overlay = document.getElementsByClassName('overlay')[0];
    document.getElementsByTagName('body')[0].removeChild(overlay);
  }

  updateInterface(pageFilter, sort) {
    document.getElementById('page-filter').value = pageFilter === 0 ? '' : pageFilter;

    if (sort != null) {
      document.getElementById(sort).checked = true;
    }
  }
}