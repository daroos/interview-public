export class BookRepository {
  constructor(sessionStorage, viewRenderer) {
    this.sessionStorage = sessionStorage;
    this.viewRenderer = viewRenderer;
  }

  filterAndSortBooks(books, pageFilter, sort) {
    let filteredBooks = books.filter(book => {
      return book.pages > pageFilter;
    });

    this.sessionStorage.setItem('sort', sort);
    this.sessionStorage.setItem('pageFilter', (pageFilter === 0 || pageFilter === null) ? '' : pageFilter);

    switch (sort) {
      case 'page-count':
        filteredBooks.sort((a, b) => {
          return a.pages > b.pages;
        });
        break;
      case 'publish-date':
        filteredBooks.sort((a, b) => {
          return a.releaseDate > b.releaseDate;
        });
        break;
      case 'authors-name':
        filteredBooks.sort((a, b) => {
          return a.authorsSortableSurname > b.authorsSortableSurname;
        });
        break;
      default:
        this.sessionStorage.removeItem('sort');
    }

    this.viewRenderer.updateInterface(pageFilter, sort);

    return filteredBooks;
  }
}