export class BooksApp {
  constructor(apiClient, viewRenderer, sessionStorage, bookRepository) {
    this.viewRenderer = viewRenderer;
    this.sessionStorage = sessionStorage;
    this.pageFilter = sessionStorage.getItem('pageFilter');
    this.sort = sessionStorage.getItem('sort');
    this.keyPressed = [];

    apiClient.getAllBooks().then(books => {
      this.books = books;
      viewRenderer.renderBooks(bookRepository.filterAndSortBooks(books, sessionStorage.getItem('pageFilter'), sessionStorage.getItem('sort')));
    });

    document.getElementById('page-filter').addEventListener('keyup', (event) => {
      this.pageFilter = event.target.value;
      this.viewRenderer.renderBooks(bookRepository.filterAndSortBooks(this.books, this.pageFilter, this.sort));
    });

    const sortOptions = ['page-count', 'publish-date', 'authors-name'];

    sortOptions.forEach(sortOption => {
      document.getElementById(sortOption).addEventListener('change', () => {
        this.sort = sortOption;
        this.viewRenderer.renderBooks(bookRepository.filterAndSortBooks(this.books, this.pageFilter, this.sort));
      });
    });

    document.getElementById('clear-filters').addEventListener('click', () => {
      this.clearFilters();
    });

    document.addEventListener('keydown', this.handleKeyPress.bind(this));
    document.addEventListener('keyup', this.clearKeyPressed.bind(this));
  }

  clearFilters() {
    this.filter = '';
    this.pageFilter = 0;

    document.getElementById('page-filter').value = '';

    const sorts = document.getElementsByClassName('sort');

    Array.from(sorts).forEach((item) => {
      item.checked = false;
    });

    this.viewRenderer.renderBooks(this.books);

    this.sessionStorage.removeItem('pageFilter');
    this.sessionStorage.removeItem('sort');
  }

  handleKeyPress(e) {
    e = e || event;

    this.keyPressed.push(e.which);

    if (
      (this.keyPressed.indexOf(18) > -1 && this.keyPressed.indexOf(82) > -1)
      || (this.keyPressed.indexOf(225) > -1 && this.keyPressed.indexOf(82) > -1)
    ) {
      this.clearFilters();
    }
  }

  clearKeyPressed() {
    this.keyPressed = [];
  }
}