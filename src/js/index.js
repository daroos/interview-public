'use strict';

import { BooksApp } from './BooksApp';
import {ApiClient} from "./ApiClient";
import {ViewRenderer} from "./ViewRenderer";
import {SessionStorage} from "./SessionStorage";
import {BookRepository} from "./BookRepository";

window.addEventListener(
  'load',
  () => new BooksApp(
    new ApiClient(),
    new ViewRenderer(),
    new SessionStorage(),
    new BookRepository(new SessionStorage(), new ViewRenderer())
  )
);