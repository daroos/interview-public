Rozdzielczości poniżej 550px szerokości nie są obsługiwane<br />
Aplikacja powinna działać na wszystkich wersjach Edge > 14, Firefox > 39, Chrome > 42, Safari, chociaż nie miałem możliwości przetestowania jej na Edge i Safari > 10.1<br />
W związku z dodaniem "babel-polyfill" powinna też działać na starszych przeglądarkach<br />
W związku z dziwnym zachowaniem przechwytywania klawiszy pod Linuxem Ubuntu 16.04 (altKey dla prawego alta było zawsze false) musiałem zastosować trick z przechowywaniem wciśniętych klawiszy w arrayu<br />
Podzieliłem pliki ze stylami w zależności od funkcji lub rozdzielczości, style tworzyłem w oparciu o "mobile first"<br />
Aplikacja napisana została w ES6, przed npm start dodałem etap budowania