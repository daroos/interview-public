const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const LiveReloadPlugin = require('webpack-livereload-plugin');
require('babel-polyfill');
require('whatwg-fetch');

module.exports = {
  entry: [
    'babel-polyfill',
    'whatwg-fetch',
    './src/js/index.js',
    './src/scss/index.scss'
  ],
  output: {
    path: path.resolve(__dirname, './static'),
    filename: '[name].bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['env']
          }
        }
      },
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            {
              loader: 'css-loader',
              options: {
                sourceMap: true,
                minimize: true
              }
            },
            {
              loader: 'sass-loader',
              options: {
                sourceMap: true
              }
            }
          ]
        })
      }
    ]
  },
  plugins: [
    new ExtractTextPlugin({
      filename: 'style.bundle.css'
    }),
    new LiveReloadPlugin()
  ],
  devtool: 'source-map'
};
